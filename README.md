# seam

seam is a webpage for managing event participations. It's written in Python featuring the Django Framework. We're trying to solve the problems created by organizing events via emails with the added bonus of an event calendar and better interoperability. (some of the) current features:

- Create and organize events with multiple people.
- Provide an event calendar and access to it in local calendars via a subscription.
- Send emails to attendees.
- Export event data to the spreadsheet software of your liking.
- Let attendees share their contact information with other attendees.
- Specify arrival/depature dates for each attendant.

This software is currently available in the following languages:

- English
- German

## Getting started

Clone the repository and setup your Python environment. Install dependencies with:

```bash
pip install -r requirements/development.txt
npm install
```

To get started run:

```bash
createdb seam  # postgres

# apply migrations and compile locales
python manage.py migrate
python manage.py compilemessages
```

During development run the server with

```bash
python manage.py runserver
```

To watch the js/scss files continuously use:

```bash
npm run dev
```

## Notes

seam is licensed under [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html.en).
