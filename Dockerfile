# Build javascript dependencies
FROM node AS static-builder

WORKDIR "/tmp/build"

COPY package.json /tmp/build/package.json
COPY package-lock.json /tmp/build/package-lock.json
RUN npm install

COPY webpack.config.js /tmp/build/webpack.config.js
COPY assets /tmp/build/assets

RUN npm run build

# Build python base image
FROM python:3.12 AS base

RUN apt-get update \
    && apt-get install -y locales locales-all libldap2-dev libsasl2-dev libssl-dev \
    && rm -rf /var/lib/apt/lists/*

COPY requirements /tmp/requirements
RUN pip install -r /tmp/requirements/production.txt && rm -rf /tmp/reqiurements

COPY manage.py /app/manage.py
COPY seam /app/seam
WORKDIR "/app"

# Build translations
FROM base AS i18n-builder

RUN apt-get update && apt-get install -y gettext \
    && ./manage.py compilemessages \
    && find . -name '*.mo' | xargs tar -cf /app/translations.tar


# Build final image
FROM base
COPY --from=i18n-builder /app/translations.tar /app/translations.tar
RUN tar -xf translations.tar && rm translations.tar

COPY --from=static-builder /tmp/build/dist /app/dist

ENTRYPOINT ["/bin/sh", "-c", "./manage.py collectstatic --noinput --skip-checks && daphne -b 0.0.0.0 seam.asgi:application"]
