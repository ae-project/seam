const miniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");

const path = require("path");

module.exports = {
  entry: {
    main: "./assets/js/app.js",
    "orga/table": "./assets/js/orga/table.js",
    "orga/dates": "./assets/js/orga/dates.js",
    edit: "./assets/js/edit.js",
    custom_fields: "./assets/js/custom_fields.js",
    attendance: "./assets/js/attendance.js",
    index: "./assets/js/index.js",
  },
  plugins: [new miniCssExtractPlugin()],
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, "./dist"),
  },
  module: {
    rules: [
      {
        test: /\.(s?css)$/,
        use: [
          {
            loader: miniCssExtractPlugin.loader,
          },
          {
            loader: "css-loader",
          },
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                plugins: () => [require("autoprefixer")],
              },
            },
          },
          {
            loader: "sass-loader",
          },
        ],
      },
    ],
  },
  optimization: {
    minimizer: [`...`, new CssMinimizerPlugin()],
  },
};
