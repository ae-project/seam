const display = document.getElementById("id_display");

display.addEventListener(
  "change",
  () => (window.location.href = `?display=${display.value}`),
);
