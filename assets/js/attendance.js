import { Collapse } from "bootstrap";

const radios = document.getElementsByName("custom_dates");

const collapse = new Collapse("#customDateInputs", { toggle: false });

for (let radio of radios) {
  radio.addEventListener("click", () => {
    if (radio.value === "yes") {
      collapse.show();
    } else {
      collapse.hide();
    }
  });
}
