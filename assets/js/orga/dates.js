import { Chart } from "chart.js/auto";

const data = JSON.parse(
  document.getElementById("attendance-dates").textContent,
);

const ctx = document.getElementById("attendanceChart");

const style = getComputedStyle(document.body);

new Chart(ctx, {
  type: "bar",
  data: {
    labels: data.x,
    datasets: [
      {
        label: data.title,
        data: data.y,
        borderColor: style.getPropertyValue("--bs-primary"),
        backgroundColor: style.getPropertyValue("--bs-primary-border-subtle"),
        borderWidth: 1,
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      y: {
        beginAtZero: true,
        ticks: {
          stepSize: 1,
        },
      },
    },
    animation: false,
  },
});
