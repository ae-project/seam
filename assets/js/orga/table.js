import DataTable from "datatables.net-bs5";
import "datatables.net-buttons-bs5";
import "datatables.net-buttons/js/buttons.colVis.mjs";
import "datatables.net-buttons/js/buttons.print.mjs";
import "datatables.net-responsive-bs5";
import "datatables.net-fixedheader-bs5";
import "datatables.net-searchbuilder-bs5";
import "datatables.net-searchpanes-bs5";
import "datatables.net-select-bs5";
import "datatables.net-staterestore-bs5";

import "datatables.net-bs5/css/dataTables.bootstrap5.min.css";
import "datatables.net-buttons-bs5/css/buttons.bootstrap5.min.css";
import "datatables.net-searchbuilder-bs5/css/searchBuilder.bootstrap5.min.css";
import "datatables.net-searchpanes-bs5/css/searchPanes.bootstrap5.min.css";

import languageDE from "datatables.net-plugins/i18n/de-DE.mjs";
import languageFR from "datatables.net-plugins/i18n/fr-FR.mjs";
import languageES from "datatables.net-plugins/i18n/es-ES.mjs";

// Parse our attendance data from the (json) script tag created by django
const attendance = JSON.parse(
  document.getElementById("attendance-data").textContent,
);

/**
 * Translated names of the columns
 */
const columnMapping = JSON.parse(
  document.getElementById("column-naming-data").textContent,
);

/**
 * Same as columnMapping but for our organizer defined fields
 */
const customFieldMapping = JSON.parse(
  document.getElementById("custom-columns-data").textContent,
);

const statusMapping = JSON.parse(
  document.getElementById("status-naming-data").textContent,
);

/**
 * Color Mapping for attendance status
 */
const colorMapping = {
  created: "bg-secondary",
  accepted: "bg-success",
  waiting: "bg-warning",
  cancelled: "bg-danger",
};

/**
 * Mapping of browser language to gridjs locale
 */
const languageMapping = {
  de: languageDE,
  fr: languageFR,
  es: languageES,
};

const language = languageMapping[navigator.languages[0].substring(0, 2)];

const columns = [
  {
    name: "full_name",
    title: columnMapping["full_name"],
    render: (data, type, row, meta) =>
      `<span>${row["first_name"]} ${row["last_name"]}` +
      (row["pronoun"] ? ` (${row["pronoun"]})` : "") +
      "</span>",
    type: "html",
  },
  {
    data: "first_name",
    name: "first_name",
    title: columnMapping["first_name"],
    visible: false,
  },
  {
    data: "last_name",
    name: "last_name",
    title: columnMapping["last_name"],
    visible: false,
  },
  {
    data: "pronoun",
    name: "pronoun",
    title: columnMapping["pronoun"],
    visible: false,
    orderable: false,
  },
  {
    data: "email_address",
    name: "email_address",
    title: columnMapping["email_address"],
    render: (data, type, row, meta) =>
      `<small><a href="mailto:${data}">${data}</a></small>` +
      (row["email_verified"]
        ? '<small>&nbsp;<i class="bi bi-check2-circle text-secondary" title="Email verified"></i></small>'
        : ""),
    orderable: false,
    type: "html",
  },
  {
    data: "phone_number",
    name: "phone_number",
    title: columnMapping["phone_number"],
    className: "text-start",
    render: (data) => `<small><a href="tel:${data}">${data}</a></small>`,
    orderable: false,
    type: "html",
    defaultContent: "-",
  },
  { data: "city", name: "city", title: columnMapping["city"] },
  {
    data: "age",
    name: "age",
    title: columnMapping["age"],
    render: DataTable.render.number(),
    visible: false,
    type: "num",
    defaultContent: "-",
  },
  {
    data: "created_at",
    name: "created_at",
    title: columnMapping["created_at"],
    render: DataTable.render.datetime(),
    visible: false,
    searchable: false,
    type: "date",
  },
  {
    data: "food_intolerances",
    name: "food_intolerances",
    title: columnMapping["food_intolerances"],
    visible: false,
  },
  {
    data: "status",
    name: "status",
    title: columnMapping["status"],
    className: "text-center",
    render: (data, type, row, meta) =>
      `<span class="badge ${colorMapping[data]}">${statusMapping[data]}</span>`,
    type: "html",
    visible: true,
  },
  {
    data: "link",
    name: "link",
    title: "Link",
    className: "text-center",
    render: (data, type, row, meta) => `<a href="${data}">Edit</a>`,
    orderable: false,
    searchable: false,
    visible: true,
  },
  {
    data: "email_verified",
    title: columnMapping["email_verified"],
    visible: false,
  },
];

const visibiltyToggleColumns = [
  "full_name:name",
  "first_name:name",
  "last_name:name",
  "pronoun:name",
  "email_address:name",
  "phone_number:name",
  "city:name",
  "age:name",
  "created_at:name",
  "food_intolerances:name",
];

const searchBuilderColumns = [
  "first_name:name",
  "last_name:name",
  "pronoun:name",
  "email_address:name",
  "phone_number:name",
  "city:name",
  "status:name",
  "age:name",
];

const searchPaneColumns = ["city:name", "status:name", "age:name"];

// Here we add the fields defined by the organizers
const customColumns = [];
for (const fieldname in customFieldMapping) {
  customColumns.push({
    data: fieldname,
    name: fieldname,
    title: customFieldMapping[fieldname],
    visible: false,
  });

  const selector = `${fieldname}:name`;
  visibiltyToggleColumns.push(selector);
  searchBuilderColumns.push(selector);
  searchPaneColumns.push(selector);
}

// We add our custom columns before the link and status columns
columns.splice(-3, 0, ...customColumns);

new DataTable("#attendanceTable", {
  // overflow in x direction and enable scrolling
  scrollX: true,
  // cell selection in the table is disabled
  select: false,
  // save the filter options, ordering, etc. to localstorage
  stateSave: true,
  // Add language mapping based on the browser
  language: language,
  // The data is already ordered by `created_date`
  // We do this for the order indication, when the column is visible
  order: { name: "created_at", dir: "desc" },
  layout: {
    bottomEnd: "paging",
    bottomStart: "pageLength",
    bottom2End: "info",
    topEnd: "search",
    top2: {},
    topStart: {
      buttons: [
        {
          extend: "colvis",
          columns: visibiltyToggleColumns,
          postfixButtons: ["colvisRestore"],
        },
        {
          extend: "searchBuilder",
          config: {
            columns: searchBuilderColumns,
          },
        },
        {
          extend: "searchPanes",
          config: {
            dtOpts: {
              select: {
                style: "multi",
              },
            },
            panes: [
              // Filter based on email verification status
              {
                header: columnMapping["email_verified"],
                options: [
                  {
                    label: columnMapping["email_verified"],
                    value: (data, idx) => data["email_verified"],
                  },
                  {
                    label: columnMapping["email_unverified"],
                    value: (data, idx) => !data["email_verified"],
                  },
                ],
              },
              // Filter by age (adult, young adult, youth)
              {
                header: columnMapping["age"],
                options: [
                  {
                    label: "< 18",
                    value: (data) => data["age"] < 18,
                  },
                  {
                    label: "18 - 27",
                    value: (data) => data["age"] >= 18 && data["age"] <= 27,
                  },
                  {
                    label: "> 27",
                    value: (data) => data["age"] > 27,
                  },
                  {
                    label: "n/a",
                    value: (data) => data["age"] === null,
                  },
                ],
              },
            ],
            columns: searchPaneColumns,
          },
        },
        "print",
      ],
    },
  },
  columns: columns,
  data: attendance,
});
