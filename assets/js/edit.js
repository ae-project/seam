/** The following code aims
 * - to disable all functionality regarding registrations if they are managed externally.
 * - to prevent organizers from locking themself out of their events
 */

import { Modal } from "bootstrap";

const form = document.getElementById("editForm");

// Registration checkbox handling
const checkStatic = form.elements["static_event"];
const checkOpenRegistration = form.elements["open_registration"];
const checkWaitlist = form.elements["waitlist_registration"];
const checkCustomAttendance = form.elements["custom_attendance_dates"];
const checkRequireAge = form.elements["require_age"];
const checkAutoAcceptRegistrations = form.elements["auto_accept_registrations"];
const checkShowAttendanceCount = form.elements["show_attendance_count"];

function updateButtons(trigger, elements, inverted = false) {
  if (trigger.checked ^ inverted) {
    for (const element of elements) {
      element.checked = false;
      element.disabled = true;
    }
  } else {
    elements.forEach((element) => (element.disabled = false));
  }
}

const staticUpdate = () =>
  updateButtons(checkStatic, [
    checkOpenRegistration,
    checkWaitlist,
    checkCustomAttendance,
    checkRequireAge,
    checkAutoAcceptRegistrations,
    checkShowAttendanceCount,
  ]);
staticUpdate();
checkStatic.addEventListener("change", staticUpdate);

const registrationUpdate = () =>
  updateButtons(checkOpenRegistration, [checkWaitlist], true);
registrationUpdate();
checkOpenRegistration.addEventListener("change", registrationUpdate);

// Lockout handling, we skip this for non superusers
// We assume they know what they're doing and have
// access to the events anyway.
const organizerList = form.elements["organizers"];
const userIDElement = document.getElementById("user_id");

if (userIDElement) {
  const userID = JSON.parse(userIDElement.textContent);
  const organizerWarningModal = new Modal(
    document.getElementById("organizerWarningModal"),
  );

  function confirmOrganizers() {
    const userIsOrganizer = Array.from(organizerList.selectedOptions)
      .map((o) => Number(o.value))
      .some((v) => v === userID);

    if (!userIsOrganizer) {
      organizerWarningModal.show();
    }
  }

  organizerList.addEventListener("change", confirmOrganizers);
}
