import { EditorView, basicSetup } from "codemirror";
import { keymap } from "@codemirror/view";
import { json, jsonParseLinter } from "@codemirror/lang-json";
import { linter, lintGutter } from "@codemirror/lint";

import { indentWithTab, defaultKeymap } from "@codemirror/commands";
import {
  indentOnInput,
  indentUnit,
  bracketMatching,
} from "@codemirror/language";
import {
  closeBrackets,
  autocompletion,
  closeBracketsKeymap,
  completionKeymap,
} from "@codemirror/autocomplete";
import { Compartment } from "@codemirror/state";
import { oneDark } from "@codemirror/theme-one-dark";

const hiddenInput = document.getElementById("id_custom_fields_schema");

const getTheme = () =>
  window.matchMedia("(prefers-color-scheme: dark)").matches ? oneDark : [];
const editorTheme = new Compartment();

const editor = new EditorView({
  doc: hiddenInput.value,
  extensions: [
    basicSetup,
    autocompletion(),
    bracketMatching(),
    closeBrackets(),
    keymap.of([
      indentWithTab,
      ...closeBracketsKeymap,
      ...defaultKeymap,
      ...completionKeymap,
    ]),
    editorTheme.of(getTheme()),
    json(),
    lintGutter(),
    linter(jsonParseLinter()),
    // Write the content back to the hidden input
    EditorView.updateListener.of((v) => {
      if (v.docChanged) {
        hiddenInput.value = v.state.doc.toString();
      }
    }),
  ],
  parent: document.getElementById("editor"),
});

// Switch to the preferred color
window
  .matchMedia("(prefers-color-scheme: dark)")
  .addEventListener("change", () => {
    editor.dispatch({
      effects: editorTheme.reconfigure(getTheme()),
    });
  });
