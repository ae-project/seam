import "../scss/styles.scss";

import * as bootstrap from "bootstrap";

// Enable bootstrap/popper popovers
const popoverTriggerList = document.querySelectorAll(
  '[data-bs-toggle="popover"]',
);
const popoverList = [...popoverTriggerList].map(
  (popoverTriggerEl) => new bootstrap.Popover(popoverTriggerEl),
);

// Update the color scheme to the requested one
const updateColorScheme = () =>
  document
    .querySelector("html")
    .setAttribute(
      "data-bs-theme",
      window.matchMedia("(prefers-color-scheme: dark)").matches
        ? "dark"
        : "light",
    );

updateColorScheme();
window
  .matchMedia("(prefers-color-scheme: dark)")
  .addEventListener("change", updateColorScheme);
