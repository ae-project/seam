from django.contrib.auth.mixins import UserPassesTestMixin
from django.core.mail import send_mail
from django.template.loader import render_to_string


class EmailMixin:
    """Small helper for sending emails."""

    def send_email(
        self,
        subject: str,
        template_name: str,
        context_data: dict,
        recipient_list: list[str],
        fail_silently: bool = False,
    ):
        """Renders the given template with context data and sends an email."""
        send_mail(
            subject=subject,
            message=render_to_string(template_name, context_data),
            from_email=None,
            recipient_list=recipient_list,
            fail_silently=fail_silently,
        )


class EventAccessMixin(UserPassesTestMixin):
    def test_func(self):
        """The detail page can only be accessed by authenticated users if it's a draft."""
        return (
            not self.get_object().draft
            or self.request.user.is_superuser
            or self.get_object()
            .organizers.filter(username=self.request.user.username)
            .exists()
        )
