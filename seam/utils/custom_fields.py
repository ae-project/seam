import json

from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.translation import gettext as _

from seam.models import Attendance


class PrettyJSONEncoder(json.JSONEncoder):
    """Prettyprints JSON."""

    def __init__(self, *args, indent, sort_keys, **kwargs):
        super().__init__(*args, indent=2, sort_keys=True, **kwargs)


EXISTING_FIELDS = set(field.name for field in Attendance._meta.get_fields())

MAPPING = {
    "boolean": lambda **field_schema: forms.BooleanField(
        **{"required": False, **field_schema}
    ),
    "text": lambda **field_schema: forms.CharField(
        **field_schema, widget=forms.Textarea
    ),
    "char": forms.CharField,
    "choice": forms.ChoiceField,
    "integer": forms.IntegerField,
    "date": forms.DateField,
    "datetime": forms.DateTimeField,
}


def validate_schema(schema):
    """Validates the schema for custom fields.

    TODO (till): Use pyparsing for better error handling and line number reporting in the future.
    """

    validate_field_name = RegexValidator(
        regex="^[-a-zA-Z0-9_]+\Z",
        message=_(
            'Field name ("%(value)s") must only contain letters, digits, hyphens and underscores.'
        ),
        code="invalid",
    )

    for field_name, field_schema in schema.items():
        # Check if field_name is a slug
        validate_field_name(field_name)

        # Require all fields to be dictionaries
        if not isinstance(field_schema, dict):
            raise ValidationError(
                _(
                    'The configuration of the field "%(field_name)" must be a dictionary.'
                ),
                params={"field_name": field_name},
            )

        # Check if all required attributes are set
        required_attributes = {"label", "help_text", "type"}
        if not required_attributes.issubset(field_schema.keys()):
            raise ValidationError(
                _(
                    f'The field "%(field_name)s" requires the following attributes: ({", ".join(required_attributes)})'
                ),
                params={"field_name": field_name},
            )

        # Check if type attribute exists in mapping
        if field_schema["type"] not in MAPPING.keys():
            raise ValidationError(
                _(
                    f'The type of field "%(field_name)s" must be one of: ({", ".join(MAPPING.keys())})'
                ),
                params={"field_name": field_name},
            )

        # Check if string attributes are actually strings
        string_attributes = {"help_text", "label"}
        if not all(
            (
                isinstance(field_schema[attribute], str)
                for attribute in string_attributes
            )
        ):
            raise ValidationError(
                _(
                    f'The attributes ({", ".join(string_attributes)}) of field "%(field_name)s" must be strings.'
                ),
                params={"field_name": field_name},
            )

        # Check if the field tries to override an existing field on the model
        if field_name in EXISTING_FIELDS:
            raise ValidationError(
                _('The field name "%(field_name)s" is already taken.'),
                params={"field_name": field_name},
            )

        # Lastly we simply try to instantiate the field and pass the error
        try:
            map_field(field_schema)
        except Exception as e:
            raise ValidationError(
                _("The field %(value)s"),
                params={"value": str(e).replace("Field.__init__()", field_name)},
            )


def map_field(field_schema):
    """Maps the field type to a FormField."""
    field_schema = field_schema.copy()
    return MAPPING[field_schema.pop("type")](**field_schema)
