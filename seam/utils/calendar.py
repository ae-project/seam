from calendar import LocaleHTMLCalendar
from datetime import date, timedelta

from django.utils.timezone import localdate
from django.utils.translation import get_language, to_locale

from seam.models import Event


def safe_locale():
    """The following code can fail on systems with weird locales.

    Then we just ignore the problem.
    """
    try:
        return to_locale(get_language()) + ".utf-8"
    except Exception:
        return None


class Calendar(LocaleHTMLCalendar):
    cssclass_month = "calendar w-100"
    event_css = "calendar-highlighted bg-opacity-25 bg-primary "

    def __init__(self, *args, event=Event, locale=safe_locale(), **kwargs):
        super().__init__(*args, locale=locale, **kwargs)

        self.event = event

        self.start = localdate(self.event.start)
        self.end = localdate(self.event.end)

        self.found_start = False

    def is_in_event(self, day: int, lookahead: bool = False) -> bool:
        """Returns true if the given day overlaps with the event.

        If lookahead is true, we look at the given day + 1.
        """
        current = date(self.year, self.month, day)
        if lookahead:
            current += timedelta(1)
        return current <= self.end and current >= self.start

    def formatmonth(self, theyear, themonth, withyear=True):
        """Formats a given month/year as html."""
        self.year = theyear
        self.month = themonth

        return super().formatmonth(theyear, themonth, withyear=withyear)

    def formatday(self, day, weekday):
        if day == 0:
            # day outside month
            return f'<td class="{self.cssclass_noday}">&nbsp;</td>'
        else:
            more = self.event_css if self.is_in_event(day) else ""

            # If the event is in the day and we haven't found the start yet, we found it now.
            if more and not self.found_start:
                self.found_start = True
                more += "rounded-start-4 start "
            # Check the next day to find the end while we're in the event
            if self.found_start and not self.is_in_event(day, lookahead=True):
                more += "rounded-end-4 end "

            return f'<td class="{more + self.cssclasses[weekday]}">{day}</td>'

    def render(self):
        """Renders all months relevant for the given date."""
        # Allows for calling render multiple times
        self.found_start = False

        return (
            self.formatmonth(year, month)
            for year in range(self.start.year, self.end.year + 1)
            for month in range(
                self.start.month if year == self.start.year else 1,
                self.end.month + 1 if year == self.end.year else 13,
            )
        )
