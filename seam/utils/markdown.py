from collections.abc import Sequence
from typing import Any

from markdown_it.renderer import RendererProtocol
from markdown_it.token import Token
from markdown_it.utils import EnvType, OptionsDict


class MarkdownRendererText(RendererProtocol):
    """Renders given markdown text as text without symbols."""

    __output__ = "text"

    def __init__(self, parser: Any = None):
        pass

    def render(
        self, tokens: Sequence[Token], options: OptionsDict, env: EnvType
    ) -> str:
        """Render function for markdown to text.

        Note: This does not handle lists and links, but should suffice for stripping
        headings. This is only used for the event preview on the index page.
        """
        self.result = ""

        for i, token in enumerate(tokens):
            if token.type == "inline" and token.children:
                for child_token in token.children:
                    self.append(child_token)
            else:
                self.append(token)

        return self.result

    def append(self, token):
        """Append the content of the token if it's shown and text."""
        if token.type == "text" and not token.hidden:
            self.result += token.content + " "
