from seam.models import NegativePhrase


def clean_text_field(text: str | None) -> str:
    """
    Clean a text field by checking if it only contains negative phrases.
    If so, return an empty string. Otherwise, return the original text.
    """
    if not text:
        return ""

    text = text.strip().lower()

    # Get all negative phrases from the database
    negative_phrases = set(
        phrase.phrase.lower() for phrase in NegativePhrase.objects.all()
    )

    # Check if the text consists only of negative phrases, punctuation, or whitespace
    words = set(word.strip(".,/-") for word in text.split())

    # If all words are negative phrases or empty strings, return empty string
    if all(word in negative_phrases or not word for word in words):
        return ""

    return text
