from datetime import timedelta

from django.test import TestCase
from django.utils.timezone import now

from seam.forms import AttendanceCreateForm
from seam.models import Event, NegativePhrase


class AttendanceFormTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Create an event
        cls.event = Event.objects.create(
            title="Test Event",
            description="Test Description",
            start=now(),
            end=now() + timedelta(days=1),
            place="Test Place",
            max_participants=10,
        )

        # Create some negative phrases
        phrases = ["nein", "keine", "nö", "/"]
        NegativePhrase.objects.bulk_create(
            (NegativePhrase(phrase=phrase) for phrase in phrases), ignore_conflicts=True
        )

    def test_negative_phrase_cleaning(self):
        # Test cases with expected results
        test_cases = [
            # Single negative phrases
            ("nein", ""),
            ("keine", ""),
            ("nö", ""),
            ("/", ""),
            # Negative phrases with punctuation
            ("nein.", ""),
            ("keine,", ""),
            # Mixed case
            ("Nein", ""),
            ("KeInE", ""),
            # Multiple negative phrases
            ("nein, keine", ""),
            # Valid text should remain unchanged
            ("I have a lactose intolerance", "i have a lactose intolerance"),
            ("Vegetarian", "vegetarian"),
            # Mixed valid and negative (should keep text)
            ("Nein, but I'm vegetarian", "nein, but i'm vegetarian"),
        ]

        for input_text, expected in test_cases:
            # Test both notes and food_intolerances fields
            form_data = {
                "first_name": "Test",
                "last_name": "User",
                "email_address": "test@example.com",
                "notes": input_text,
                "food_intolerances": input_text,
            }

            form = AttendanceCreateForm(data=form_data, event=self.event)
            self.assertTrue(
                form.is_valid(), f"Form should be valid with input: {input_text}"
            )

            # Check that both fields are cleaned correctly
            cleaned_data = form.cleaned_data
            self.assertEqual(
                cleaned_data["notes"],
                expected,
                f"Notes field not cleaned correctly for input: {input_text}",
            )
            self.assertEqual(
                cleaned_data["food_intolerances"],
                expected,
                f"Food intolerances field not cleaned correctly for input: {input_text}",
            )
