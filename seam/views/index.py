from typing import assert_never

from django.conf import settings
from django.db.models import Q
from django.utils.timezone import localtime
from django.views.generic.list import ListView

from seam.forms import EventSearchForm
from seam.models import Event


class HomeView(ListView):
    """Home page with event overviews and navigation."""

    model = Event
    template_name = "seam/home.html"

    @property
    def qs(self):
        """Filter out drafts and unlisted events for unauthenticated users, and non organizers."""
        if not hasattr(self, "_qs"):
            if self.request.user.is_anonymous:
                self._qs = Event.objects.filter(draft=False, unlisted=False)
            elif self.request.user.is_superuser:
                self._qs = Event.objects.all()
            elif not self.request.user.is_superuser:
                # Logged in users can only see drafts and unlisted events if they organize them.
                self._qs = Event.objects.exclude(
                    Q(draft=True) & ~Q(organizers__username=self.request.user.username)
                ).exclude(
                    Q(unlisted=True)
                    & ~Q(organizers__username=self.request.user.username)
                )
            else:
                assert_never()  # Fallthrough

        return self._qs

    def get_queryset(self):
        """We're doing a weird kind of pagination.

        Our numbering system can be negative and our 0-page is centered around the current date.
        Events that lay in the past are found by negative numbers, events that end in the future are found
        on pages >= 0.
        """

        self.page = 0
        self.pagesize = settings.PAGE_SIZE_CARDS
        self.display = "cards"

        self.root = localtime()

        if self.request.GET:
            self.form = EventSearchForm(self.request.GET)
            if self.form.is_valid():
                self.page = self.form.cleaned_data["page"] or self.page
                self.display = self.form.cleaned_data["display"] or "cards"
                self.pagesize = (
                    self.pagesize
                    if self.display == "cards"
                    else settings.PAGE_SIZE_TABLE
                )
        else:
            self.form = EventSearchForm()

        # For negative pages we need to start from 0 as well regarding our query offsets
        start = (self.page if self.page >= 0 else abs(self.page + 1)) * self.pagesize
        end = start + self.pagesize

        return (
            self.qs.filter(end__gte=self.root).order_by("end")[start:end]
            if self.page >= 0
            # We have to order the other way around, but reverse the result of each page for displaying
            else reversed(self.qs.filter(end__lt=self.root).order_by("-end")[start:end])
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["form"] = self.form
        context["display"] = self.display

        # Count the amount of events up to the current page (including the page itself)
        count = (self.page + 1 if self.page >= 0 else abs(self.page)) * self.pagesize

        context["has_next"] = (
            # If there are no upcoming events, we can still navigate to the page 0
            self.page < 0 or self.qs.filter(end__gte=self.root).count() > count
        )

        context["has_prev"] = (
            # current page is bigger than 0, there has to be a page before that
            self.page > 0
            # the current page is 0, but Events in the past exist
            or (self.page == 0 and self.qs.filter(end__lt=self.root).exists())
            # current page is somehwere in the past
            or self.qs.filter(end__lt=self.root).count() >= count
        )

        # Pass the current page to the template
        context["page"] = self.page

        return context
