from datetime import timedelta

from django.conf import settings
from django.http import HttpResponse
from django.urls import reverse
from django.utils.timezone import localdate, now
from django.views.generic.detail import BaseDetailView
from django.views.generic.list import BaseListView
from icalendar import Calendar
from icalendar import Event as IEvent
from icalendar import vText

from seam.mixins import EventAccessMixin
from seam.models import Event


class CalendarMixin:
    def event_to_ics(self, event: Event) -> IEvent:
        """Turns an event into an ics calendar entry."""
        e = IEvent()
        e.add("summary", event.title)

        e["location"] = vText(event.place)
        e.add("description", event.description)
        e.add(
            "url",
            self.request.build_absolute_uri(
                reverse("event_detail", kwargs={"pk": event.pk})
            ),
        )

        # Only set the date if this is an „whole day event“
        if event.whole_day_event:
            e.add("dtstart", localdate(event.start))
            e.add("dtend", localdate(event.end))
        else:
            e.add("dtstart", event.start)
            e.add("dtend", event.end)

        return e

    def get_base_calendar(self):
        """Returns the base of our calendar structure."""
        calendar = Calendar()
        calendar.add(
            "PRODID", f"-//{settings.NAVBAR_DESCRIPTION}//{self.request.get_host()}//"
        )
        calendar.add("VERSION", "2.0")

        return calendar


class EventCalendarView(EventAccessMixin, CalendarMixin, BaseDetailView):
    """Return an ics file for the given event."""

    model = Event

    def get(self, *args, **kwargs):
        calendar = self.get_base_calendar()
        calendar.add_component(self.event_to_ics(self.get_object()))

        return HttpResponse(
            calendar.to_ical(),
            content_type="text/calendar",
            headers={"Content-Disposition": "attachment; filename=event.ics"},
        )


class CalendarView(CalendarMixin, BaseListView):
    """Returns an ics calendar with all events in the future (and some in the past)."""

    model = Event

    def get(self, *args, **kwargs):
        self.queryset = self.get_queryset()

        calendar = self.get_base_calendar()

        calendar.add("NAME", settings.CALENDAR_NAME)
        calendar.add("X-WR-CALNAME", settings.CALENDAR_NAME)

        # Ignore events that are older than one year.
        for event in self.queryset.filter(
            draft=False, unlisted=False, start__gte=now() - timedelta(days=365)
        ):
            calendar.add_component(self.event_to_ics(event))

        return HttpResponse(
            calendar.to_ical(),
            content_type="text/calendar",
        )
