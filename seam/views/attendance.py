from datetime import timedelta

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin
from django.core.exceptions import PermissionDenied
from django.forms import BaseModelForm
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.utils.timezone import now
from django.utils.translation import gettext as _
from django.views.generic import FormView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from seam.forms import (
    AttendanceCreateForm,
    AttendanceEditForm,
    AttendanceFindForm,
    AttendanceStatusUpdateForm,
)
from seam.mixins import EmailMixin
from seam.models import Attendance, Event, Status


class AttendanceBaseView:
    """Gets an attendance based on the signed value"""

    model = Attendance

    def get_object(self, queryset=None):
        return Attendance.get_from_access_id(self.kwargs["attendance"])

    def get_full_url(self, view_name: str) -> str:
        """Returns a full url (with protocol) to a an attendance.

        The attendance is identified by a (secret) access id."""

        path = reverse(
            view_name,
            kwargs={"attendance": self.object.generate_access_id()},
        )

        return self.request.build_absolute_uri(path)


class AttendanceCreateOrEditBaseView(AttendanceBaseView, EmailMixin):
    object_name = "attendance"
    template_name = "seam/attendance_form.html"

    def get_form_kwargs(self):
        """Pass our event to the form."""
        kwargs = super().get_form_kwargs()
        kwargs.update({"user": self.request.user})
        return kwargs


class AttendanceCreateView(AttendanceCreateOrEditBaseView, CreateView):
    """Creates a new attendance object. Manages the registration if you want."""

    form_class = AttendanceCreateForm

    @property
    def event(self):
        """Returns the corresponding event from the url pk."""
        if not hasattr(self, "_event"):
            self._event = get_object_or_404(Event, pk=self.kwargs["pk"])

            # Registration is not open, we return a 404
            # Checking this here makes it prone to race conditions.
            # This could be done in a transaction while saving the attendance.
            if not self._event.can_register():
                raise Http404("Registration is currently not possible!")

        return self._event

    def get_form_kwargs(self):
        """Pass our event to the form."""
        kwargs = super().get_form_kwargs()
        kwargs.update({"event": self.event})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["event"] = self.event
        context["is_edit"] = False
        return context

    def form_valid(self, form):
        """Update our attendance with the correct event and status."""
        self.object = form.save(commit=False)

        # Store the custom fields data on the attendance
        self.object.custom_fields_data = form.cleaned_data.get(
            "custom_fields_data", None
        )

        # Set the event and the status to created
        self.object.event = self.event
        self.object.status = Status.CREATED

        self.object.save()

        # If the email address was verified during `settings.VERIFICATION_GRACE_INTERVAL`
        # it must not be verified again.
        if Attendance.objects.filter(
            email_address=self.object.email_address,
            email_verified=True,
            created_at__gte=now()
            - timedelta(days=settings.VERIFICATION_GRACE_INTERVAL * 30),
        ).exists():
            self.send_email(
                subject=_("Registration for {event}").format(
                    event=self.object.event.title
                ),
                template_name="seam/email/registration_info.txt",
                context_data={
                    "first_name": self.object.first_name,
                    "last_name": self.object.last_name,
                    "title": self.object.event.title,
                    "link": self.get_full_url("attendance_detail"),
                },
                recipient_list=[self.object.email_address],
            )

            # We're lazy and sending the user to the confirm view that sets the verification status
            return redirect(self.get_full_url("attendance_confirm"))
        else:
            # Send the verification email
            self.send_email(
                subject=_("Registration for {event}").format(
                    event=self.object.event.title
                ),
                template_name="seam/email/registration_confirm.txt",
                context_data={
                    "first_name": self.object.first_name,
                    "last_name": self.object.last_name,
                    "title": self.object.event.title,
                    "link": self.get_full_url("attendance_confirm"),
                },
                recipient_list=[self.object.email_address],
            )

            messages.add_message(
                self.request,
                messages.WARNING,
                _(
                    "Confirm your email address!"
                    " You received an email with a link to confirm and edit your registration."
                    " The registration will be deleted after %(age)sh"
                    " if the email address is not verified."
                )
                % {"age": settings.MAX_UNVERIFIED_ATTENDANCE_AGE},
            )

            return redirect(reverse("event_detail", kwargs={"pk": self.event.pk}))


class AttendanceEditView(
    UserPassesTestMixin, AttendanceCreateOrEditBaseView, UpdateView
):
    """Allows attendees to edit their registration information."""

    form_class = AttendanceEditForm

    def test_func(self):
        return (
            self.get_object().event.can_access_admin(self.request.user)
            or self.get_object().status != Status.CANCELLED
        )

    def get_form_kwargs(self):
        """Pass our event to the form."""
        kwargs = super().get_form_kwargs()
        kwargs.update({"event": self.get_object().event})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["is_edit"] = True
        return context

    def get_success_url(self):
        messages.success(self.request, _("The registration has been updated."))
        return self.object.get_absolute_url()

    def form_valid(self, form: BaseModelForm) -> HttpResponse:
        # Store the custom fields data on the attendance
        self.object.custom_fields_data = form.cleaned_data.get(
            "custom_fields_data", None
        )

        # The email address can only be changed by admin users
        if "email_address" in form.changed_data:
            self.object.email_verified = Attendance.objects.filter(
                email_address=form.cleaned_data["email_address"],
                email_verified=True,
                created_at__gte=now()
                - timedelta(days=settings.VERIFICATION_GRACE_INTERVAL * 30),
            ).exists()
        result = super().form_valid(form)

        # Send an email to organizers about updated attendance details.
        # Filter for users with enabled email notifications
        obj = self.get_object()
        event = obj.event
        if recipients := event.organizers.filter(
            profile__receive_email_notifications=True
        ).values_list("email", flat=True):
            self.send_email(
                subject=_(
                    "Registration of {attendee_first_name} {attendee_last_name} for {event} updated"
                ).format(
                    attendee_first_name=obj.first_name,
                    attendee_last_name=obj.last_name,
                    event=event.title,
                ),
                template_name="seam/email/attendance_updated.txt",
                context_data={
                    "title": event.title,
                    "link": self.get_full_url("attendance_detail"),
                },
                recipient_list=recipients,
            )

        if "email_address" in form.changed_data:
            if not self.object.email_verified:
                self.send_email(
                    subject=_("Registration for {event}").format(
                        event=self.object.event.title
                    ),
                    template_name="seam/email/registration_confirm.txt",
                    context_data={
                        "first_name": self.object.first_name,
                        "last_name": self.object.last_name,
                        "title": self.object.event.title,
                        "link": self.get_full_url("attendance_confirm"),
                    },
                    recipient_list=[self.object.email_address],
                )

                messages.add_message(
                    self.request,
                    messages.INFO,
                    _(
                        "A request for confirmation was sent to the new email address!"
                        " The registration will be deleted after %(age)sh"
                        " if the email address is not verified."
                    )
                    % {"age": settings.MAX_UNVERIFIED_ATTENDANCE_AGE},
                )

        return result


class AttendanceConfirmView(AttendanceBaseView, EmailMixin, DetailView):
    """Sets the status of a created attendance to verified. This is done in order to verify the email address."""

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        attendance = self.object
        event = attendance.event

        if not attendance.email_verified:
            attendance.email_verified = True

            # If the event is set to auto_accept mode, set the status of the attendance to either accepted or waiting
            if event.auto_accept_registrations and attendance.status == Status.CREATED:
                attendance.status = (
                    Status.ACCEPTED
                    if event.get_attendance_count() < event.max_participants
                    else Status.WAITING
                )

            attendance.save()

            # If one attendance was created and verified send an email to the organizers.
            # Given they ignore the attendance, no more emails will be sent on the same day,
            # but if they update the status there will be another email for the next verified attendance.
            # This honours the email settings in the users profiles
            if (
                event.attendances.filter(
                    email_verified=True,
                    created_at__gte=now().replace(hour=0, minute=0, second=0),
                ).count()
                == 1
            ):
                # Filter for users with enabled email notifications
                if recipients := event.organizers.filter(
                    profile__receive_email_notifications=True
                ).values_list("email", flat=True):
                    self.send_email(
                        subject=_("New registration for {event}").format(
                            event=event.title
                        ),
                        template_name="seam/email/attendance_created.txt",
                        context_data={
                            "title": event.title,
                            "link": self.get_full_url("attendance_detail"),
                        },
                        recipient_list=recipients,
                    )

        return redirect(attendance.get_absolute_url())


class AttendanceView(AttendanceBaseView, UpdateView, EmailMixin):
    """Overview of a attendance to an event"""

    template_name = "seam/attendance_detail.html"
    context_object_name = "attendance"

    form_class = AttendanceStatusUpdateForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["admin"] = self.object.event.can_access_admin(self.request.user)
        context["has_next_new"] = (
            self.object.event.attendances.filter(
                status=Status.CREATED, email_verified=True
            )
            .exclude(pk=self.object.pk)
            .exists()
        )

        if (
            self.object.status == Status.ACCEPTED
            and self.object.share_contact_information
        ):
            context["shared_attendances"] = (
                self.object.event.attendances.filter(
                    status=Status.ACCEPTED, share_contact_information=True
                )
                .exclude(pk=self.object.pk)
                .order_by("first_name")
            )

        context["attendee_can_edit"] = self.object.status != Status.CANCELLED
        return context

    def get_success_url(self):
        """Return the success url after form change.

        If there is a 'next' GET parameter specified, return the next attendance for the event.
        Used for fast edit button.

        We only do that for legitimate requests from organizers or admins.
        """
        if (
            self.object.event.can_access_admin(self.request.user)
            and "next" in self.request.GET
        ):
            if next_attendance := (
                self.object.event.attendances.filter(
                    status=Status.CREATED, email_verified=True
                )
                .order_by("created_at")
                .first()
            ):
                return next_attendance.get_absolute_url()
        return super().get_success_url()

    def form_valid(self, form):
        # Check if admin user or new status is cancelled
        if not (
            self.object.event.can_access_admin(self.request.user)
            or self.object.status == Status.CANCELLED
        ):
            raise PermissionDenied("Not allowed to edit attendance")

        if "status" in form.changed_data:
            self.send_email(
                subject=_("Registration for event {event} updated").format(
                    event=self.object.event.title
                ),
                template_name="seam/email/registration_update.txt",
                context_data={
                    "first_name": self.object.first_name,
                    "last_name": self.object.last_name,
                    "status": self.object.get_status_display(),
                    "title": self.object.event.title,
                    "link": self.get_full_url("attendance_detail"),
                },
                recipient_list=[self.object.email_address],
            )

        return super().form_valid(form)


class AttendanceDeleteView(UserPassesTestMixin, DeleteView):
    model = Attendance

    def test_func(self):
        return self.get_object().event.can_access_admin(self.request.user)

    def get_success_url(self):
        return reverse("event_organization", kwargs={"pk": self.object.event.pk})


class AttendanceFindView(FormView, EmailMixin):
    """Allows users to find events they registered for."""

    form_class = AttendanceFindForm
    template_name = "seam/attendance_find.html"
    success_url = "/"

    def form_valid(self, form):
        # Filter for recent or future attendances with a verified email address
        attendances = Attendance.objects.filter(
            email_verified=True,
            email_address=form.cleaned_data["email_address"],
            event__end__gte=now() - timedelta(weeks=26),  # 6 months
        )

        # If there are attendances, we send an email to the user.
        if attendances:
            self.send_email(
                subject=_("Links to events you registered for"),
                template_name="seam/email/attendance_find.txt",
                context_data={
                    "attendances": (
                        (
                            attendance,
                            self.request.build_absolute_uri(
                                attendance.get_absolute_url()
                            ),
                        )
                        for attendance in attendances
                    ),
                },
                recipient_list=[form.cleaned_data["email_address"]],
            )

        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                "An email has been sent to you if you have any registrations on this site."
            ),
        )

        return super().form_valid(form)
