from datetime import datetime

from django.contrib.auth.mixins import UserPassesTestMixin
from django.forms.models import model_to_dict
from django.http import HttpResponse, HttpResponseBadRequest
from django.urls import reverse
from django.utils.text import slugify
from django.utils.timezone import make_naive
from django.utils.translation import gettext as _
from django.views.generic.detail import BaseDetailView, DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from markdown_it import MarkdownIt
from tablib import Dataset

from seam.forms import EventExportForm, EventForm
from seam.mixins import EventAccessMixin
from seam.models import Attendance, Event, Image, Status
from seam.utils.calendar import Calendar


class EventDetailView(EventAccessMixin, DetailView):
    """Shows the detail page of an event to unauthenticated users."""

    model = Event
    template_name = "seam/event_detail.html"
    context_object_name = "event"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["admin"] = self.object.can_access_admin(self.request.user)

        context["calendar"] = Calendar(event=self.object).render()

        # Add lambda function to optionally render the description as markdown
        md = MarkdownIt("commonmark")
        context["markdown"] = lambda: md.render(self.object.description)

        return context


class EventBaseView(UserPassesTestMixin):
    model = Event
    context_object_name = "event"

    def test_func(self):
        return self.get_object().can_access_admin(self.request.user)


class EventOrganizerView(EventBaseView, DetailView):
    """Shows details about the event to organizers and superusers."""

    template_name = "seam/event_organizer_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["form"] = EventExportForm()

        context["attendances"] = [
            {
                **model_to_dict(
                    attendance,
                    exclude={
                        "custom_fields_data",
                        "id",
                        "notes",
                        "share_contact_information",
                    },
                ),
                **attendance.get_custom_fields(),
                "link": reverse(
                    "attendance_detail",
                    kwargs={"attendance": attendance.generate_access_id()},
                ),
                "created_at": attendance.created_at.isoformat(),
            }
            for attendance in self.object.attendances.all().order_by("-created_at")
        ]

        # Export verbose field names to frontend for i18n of table
        context["column_mapping"] = {
            field.name: field.verbose_name for field in Attendance._meta.get_fields()
        }
        # Add additional context for columns that are generated in the frontend
        context["column_mapping"].update(
            {"full_name": _("Full name"), "email_unverified": _("Email unverified")}
        )
        context["custom_columns"] = self.object.get_custom_field_names()

        # Export verbose names of the attendance status for i18n of table
        context["status_names"] = {status[0]: status[1] for status in Status.choices}

        # Get email addresses by their status
        context["emails"] = {}
        context["emails"]["all"] = self.object.attendances.filter(
            email_verified=True
        ).values_list("email_address", flat=True)
        context["emails"]["accepted"] = self.object.attendances.filter(
            status=Status.ACCEPTED
        ).values_list("email_address", flat=True)
        context["emails"]["waitlist"] = self.object.attendances.filter(
            status=Status.WAITING
        ).values_list("email_address", flat=True)

        # Count attendances that need their status updated
        new_attendances = self.object.attendances.filter(status=Status.CREATED)

        context["new_unverified_count"] = new_attendances.filter(
            email_verified=False
        ).count()
        context["new_verified_count"] = new_attendances.filter(
            email_verified=True
        ).count()
        context["cancelled_count"] = self.object.attendances.filter(
            status=Status.CANCELLED
        )

        if self.object.custom_attendance_dates:
            context["attendance_dates"] = {
                "title": _("Amount of attendees per day"),
                "y": self.object.attendances_per_day,
                "x": self.object.days,
            }
            # FIXME: Add more statistics and display this on normal events as well.
            context["cumulative_attendance_days"] = self.object.attendance_days

        return context


class EventExportView(EventBaseView, BaseDetailView):
    """Exports all verified or filtered by status attendances to a file."""

    def get(self, *args, **kwargs):
        self.object = self.get_object()

        form = EventExportForm(self.request.GET)
        if not form.is_valid():
            return HttpResponseBadRequest("form invalid!")
        file_format = form.cleaned_data["file_format"]
        grouping = form.cleaned_data["grouping"]

        columns = [
            "first_name",
            "last_name",
            "pronoun",
            "age",
            "email_address",
            "city",
            "phone_number",
            "food_intolerances",
            "notes",
            "status",
            "created_at",
        ]

        # See make_naive for excel
        if self.object.custom_attendance_dates:
            columns += ["start", "end"]

        data = Dataset()
        data.headers = (
            Attendance._meta.get_field(column).verbose_name for column in columns
        )

        qs = self.object.attendances.filter(email_verified=True)
        if grouping != "all":
            qs = qs.filter(status=grouping)

        # Excel can't handle dates with timezone.
        # We're setting them to be based on `settings.TIME_ZONE`
        def fix_date(cell):
            return (
                cell
                if file_format != "xlsx"
                else (cell if not isinstance(cell, datetime) else make_naive(cell))
            )

        for row in qs.values_list(*columns, named=False):
            data.append([fix_date(cell) for cell in row])

        # We add the currently specified custom fields as columns to the export
        # Deleted schema fields are not visible, but still in the database
        if self.object.custom_fields_schema:
            for column, label in (
                (
                    (
                        # If the field is empty, always return None
                        fix_date(value.get(field_name, None))
                        if isinstance(value, dict)
                        else None
                        for value in qs.values_list("custom_fields_data", flat=True)
                    ),
                    field_schema["label"],
                )
                for field_name, field_schema in self.object.custom_fields_schema.items()
            ):
                data.append_col(column, header=label)

        mime_type = {
            "csv": "text/csv",
            "xlsx": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "json": "application/json",
            "ods": "application/vnd.oasis.opendocument.spreadsheet",
        }

        return HttpResponse(
            data.export(file_format),
            content_type=mime_type[file_format],
            headers={
                "Content-Disposition": "attachment; filename="
                + f'"event-{grouping}-{slugify(self.object.title)}.{file_format}"'
            },
        )


class EventBaseEditView(EventBaseView):
    form_class = EventForm
    template_name = "seam/event_edit.html"

    def get_success_url(self):
        return reverse("event_organization", kwargs={"pk": self.object.pk})

    def form_valid(self, form):
        # Images changed
        if self.object and len(form.cleaned_data["images"]) > 0:
            self.object.images.all().delete()

        result = super().form_valid(form)

        for image in form.cleaned_data["images"]:
            Image.objects.create(image_file=image, event=self.object)

        return result


class EventCreateView(EventBaseEditView, CreateView):
    def get_initial(self):
        """Add the request user as the initial organizer."""
        return {**super().get_initial(), "organizers": [self.request.user.id]}

    def test_func(self):
        """Events can be created by staff and superuser but edited by all organizers."""
        return self.request.user.is_staff or self.request.user.is_superuser


class EventUpdateView(EventBaseEditView, UpdateView):
    pass


class EventDeleteView(EventBaseView, DeleteView):
    def get_success_url(self):
        return reverse("home")
