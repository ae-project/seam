from django.conf import settings
from django.utils.translation import get_language, to_locale


def settings_processor(request):
    """Returns important settings variables to be used in templates."""

    return {
        "NAVBAR_DESCRIPTION": settings.NAVBAR_DESCRIPTION,
        "FOOTER_DESCRIPTION": settings.FOOTER_DESCRIPTION,
        "LOGO": settings.LOGO,
        "FOOTER_LINKS": settings.FOOTER_LINKS,
        "MAX_UNVERIFIED_ATTENDANCE_AGE": settings.MAX_UNVERIFIED_ATTENDANCE_AGE,
        "IMAGE_SIZES": settings.IMAGE_SIZES,
        "IMAGE_FORMATS": settings.IMAGE_FORMATS,
        "LOCALE": to_locale(get_language()),
    }
