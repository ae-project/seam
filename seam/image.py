import logging
import multiprocessing as mp
from multiprocessing.pool import Pool

# Note: Once pillow has official support, we can ditch this plugin
import pillow_avif  # noqa
from django.conf import settings
from django.db.models.query import QuerySet
from PIL import Image, ImageOps

from seam.models import Image as ImageModel

logger = logging.getLogger(__name__)

# The default start method is spawn on MacOS which makes Django unhappy
# See: https://code.djangoproject.com/ticket/31169
mp.set_start_method("fork")

BASE_PATH = settings.MEDIA_ROOT / "resized"


def resize(image: ImageModel):
    """Resizes a given image to all sizes and formats specified in the settings."""
    if image.processed:
        logger.warn(f"Image {image.pk} is already processed, proceeding anyway.")
    else:
        logger.info(f"Processing image {image.pk}.")

    # Ensure our storage folder exists
    BASE_PATH.mkdir(parents=True, exist_ok=True)

    with Image.open(image.image_file.path) as base:
        if base.format in ["GIF"]:
            image.status = "raw"
        else:
            # Prevent images in RGBA space to mess up jpeg conversion
            base = base.convert("RGB")
            for size_name, size in settings.IMAGE_SIZES.items():
                resized = ImageOps.contain(base, (size, size), Image.Resampling.LANCZOS)
                for image_format in settings.IMAGE_FORMATS:
                    resized.save(BASE_PATH / f"{image.pk}_{size_name}.{image_format}")
            image.status = "processed"
    image.save()


def process_images(images: QuerySet[ImageModel]):
    """Process a queryset of images in parallel."""
    if images.exists():
        pool = Pool(settings.IMAGE_RESIZE_POOLS)
        pool.map(resize, images)
