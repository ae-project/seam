from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path

from seam.views.attendance import (
    AttendanceConfirmView,
    AttendanceCreateView,
    AttendanceDeleteView,
    AttendanceEditView,
    AttendanceFindView,
    AttendanceView,
)
from seam.views.calendar import CalendarView, EventCalendarView
from seam.views.event import (
    EventCreateView,
    EventDeleteView,
    EventDetailView,
    EventExportView,
    EventOrganizerView,
    EventUpdateView,
)
from seam.views.index import HomeView
from seam.views.profile import ProfileView

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", HomeView.as_view(), name="home"),
    path("events/create", EventCreateView.as_view(), name="event_create"),
    path("events/<int:pk>", EventDetailView.as_view(), name="event_detail"),
    path(
        "events/<int:pk>/orga", EventOrganizerView.as_view(), name="event_organization"
    ),
    path("events/<int:pk>/export", EventExportView.as_view(), name="event_export"),
    path(
        "events/<int:pk>/register", AttendanceCreateView.as_view(), name="event_attend"
    ),
    path("events/<int:pk>/edit", EventUpdateView.as_view(), name="event_update"),
    path("events/<int:pk>/delete", EventDeleteView.as_view(), name="event_delete"),
    path(
        "events/<int:pk>/calendar", EventCalendarView.as_view(), name="event_calendar"
    ),
    # Attendance detail urls are special because of accessibility without login
    path("attendance/find", AttendanceFindView.as_view(), name="attendance_find"),
    path(
        "attendance/<str:attendance>",
        AttendanceView.as_view(),
        name="attendance_detail",
    ),
    path(
        "attendance/confirm/<str:attendance>",
        AttendanceConfirmView.as_view(),
        name="attendance_confirm",
    ),
    path(
        "attendance/<int:pk>/delete",
        AttendanceDeleteView.as_view(),
        name="attendance_delete",
    ),
    path(
        "attendance/<str:attendance>/edit",
        AttendanceEditView.as_view(),
        name="attendance_edit",
    ),
    path("accounts/login/", LoginView.as_view(), name="login"),
    path("accounts/logout/", LogoutView.as_view(), name="logout"),
    path("accounts/profile/", ProfileView.as_view(), name="profile"),
    path("calendar", CalendarView.as_view(), name="calendar"),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
