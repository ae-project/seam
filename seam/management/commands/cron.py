from datetime import timedelta

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils.timezone import now

from seam.image import process_images
from seam.models import Attendance, Image


class Command(BaseCommand):
    help = "Runs all background tasks for seam."

    def handle(self, *args, **options):
        self.stdout.write(self.style.NOTICE("Checking for unverified attendances"))
        Attendance.objects.filter(
            email_verified=False,
            created_at__lte=now()
            - timedelta(hours=settings.MAX_UNVERIFIED_ATTENDANCE_AGE),
        ).delete()

        self.stdout.write(self.style.NOTICE("Checking for unprocessed images"))
        images = Image.objects.filter(status="unprocessed")
        self.stdout.write(self.style.NOTICE(f"Processing {images.count()} images"))
        process_images(images)

        self.stdout.write("Background tasks finished.")
