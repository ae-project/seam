from django.core.management.base import BaseCommand
from faker import Faker

from seam.factory import AttendanceFactory, EventFactory


class Command(BaseCommand):
    help = "Creates fake events and attendances"

    def add_arguments(self, parser):
        parser.add_argument("amount", type=int)

    def handle(self, *args, **options):
        fake = Faker()
        attendance_factory = AttendanceFactory()

        for _ in range(0, options["amount"]):
            event = EventFactory().get()
            for _ in range(0, fake.pyint(5, 30)):
                attendance_factory.get(event)

        self.stdout.write(self.style.SUCCESS("Created fake events."))
