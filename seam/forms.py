from datetime import timedelta

from django import forms
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils.timezone import now
from django.utils.translation import gettext as _

from seam.models import Attendance, Event, Profile, Status
from seam.utils.custom_fields import PrettyJSONEncoder, map_field, validate_schema
from seam.utils.negative_phrases import clean_text_field


def configure_date_field(field, minimum=None, maximum=None):
    """Configures a given date field with correct input type and attrs."""
    field.input_formats = ["%Y-%m-%dT%H:%M"]
    field.widget.format = "%Y-%m-%dT%H:%M"
    field.widget.input_type = "datetime-local"
    field.widget.attrs.update({"step": "600"})  # Set the step to 10 Minutes

    if minimum:
        field.widget.attrs.update({"min": minimum})
    if maximum:
        field.widget.attrs.update({"max": maximum})


class EventSearchForm(forms.Form):
    page = forms.IntegerField(required=False)
    display = forms.ChoiceField(
        choices=(("cards", _("Card view")), ("table", _("Table view"))), initial="cards"
    )


class AttendanceBaseForm(forms.ModelForm):
    # Not used anywhere on the model only for hiding certain fields in the browser
    custom_dates = forms.ChoiceField(
        choices=(
            ("no", _("I'll stay the entire event.")),
            ("yes", _("I have other arrival/depature times.")),
        ),
        widget=forms.RadioSelect,
        required=False,
    )

    class Meta:
        model = Attendance
        fields = [
            "first_name",
            "last_name",
            "pronoun",
            "age",
            "email_address",
            "phone_number",
            "city",
            "food_intolerances",
            "notes",
            "share_contact_information",
            "custom_dates",
            "start",
            "end",
        ]

    def __init__(self, *args, **kwargs):
        self.event = kwargs.pop("event")
        self.user = kwargs.pop("user", None)

        super().__init__(*args, **kwargs)

        # small visual form tweaks
        self.fields["phone_number"].widget.input_type = "tel"
        self.fields["food_intolerances"].widget.attrs.update(rows=2)
        self.fields["notes"].widget.attrs.update(rows=4)

        # Set age field to be required or not
        self.fields["age"].required = self.event.require_age

        # Add custom fields to attendance form
        if self.event.custom_fields_schema:
            self.custom_fields = []
            # Instanciate the form fields and add them to the field list
            for field_name, field_schema in self.event.custom_fields_schema.items():
                field = map_field(field_schema)
                self.fields[field_name] = field
                # We keep a list of custom fields in order to render them separately in the template
                self.custom_fields.append(field_name)

        # Add custom attendance dates if the event calls for them
        if self.event.custom_attendance_dates:
            self.initial["custom_dates"] = "no"

            # Set min and max attributes to the events timeframe + a buffer
            minimum = (
                (self.event.start - timedelta(days=settings.CUSTOM_ATTENDANCE_WINDOW))
                .replace(tzinfo=None)
                .isoformat(timespec="minutes")
            )
            maximum = (
                (self.event.end + timedelta(days=settings.CUSTOM_ATTENDANCE_WINDOW))
                .replace(tzinfo=None)
                .isoformat(timespec="minutes")
            )
            for field in ("start", "end"):
                configure_date_field(
                    self.fields[field], minimum=minimum, maximum=maximum
                )
                self.fields[field].required = True

            # Prefill with events start and end dates
            if not getattr(self.instance, "start"):
                self.initial["start"] = self.event.start
            if not getattr(self.instance, "end"):
                self.initial["end"] = self.event.end
        else:
            del self.fields["start"]
            del self.fields["end"]
            del self.fields["custom_dates"]

    def _clean_date(self, name):
        """Checks if the date is in the correct timeframe."""
        date = self.cleaned_data[name]
        if date and not (date >= self.event.start and date <= self.event.end):
            raise ValidationError(
                _("The date must be within the events timeframe."), code="within"
            )
        return date

    def clean_start(self):
        return self._clean_date("start")

    def clean_end(self):
        return self._clean_date("end")

    def clean(self):
        cleaned_data = super().clean()

        # Collect all values from custom fields and store them in `custom_fields_data`
        if self.event.custom_fields_schema:
            cleaned_data["custom_fields_data"] = {
                field_name: self[field_name].value()
                for field_name in self.event.custom_fields_schema.keys()
            }

        # Check if the specified end is before the start
        # We skip this validation if we found errors on our start or end fields
        if (
            not ("start" in self.errors or "end" in self.errors)
            and self.event.custom_attendance_dates
            and cleaned_data["end"] < cleaned_data["start"]
        ):
            raise ValidationError(
                _("The end date must be bigger than the start date"), code="range"
            )

        # Clean notes and food_intolerances fields
        if "notes" in cleaned_data:
            cleaned_data["notes"] = clean_text_field(cleaned_data["notes"])
        if "food_intolerances" in cleaned_data:
            cleaned_data["food_intolerances"] = clean_text_field(
                cleaned_data["food_intolerances"]
            )

        return cleaned_data


class AttendanceCreateForm(AttendanceBaseForm):
    pass


class AttendanceEditForm(AttendanceBaseForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Only organizers can edit the email address
        can_access_admin = self.event.can_access_admin(self.user)
        self.fields["email_address"].disabled = not can_access_admin
        self.fields["email_address"].required = can_access_admin

        if not can_access_admin:
            # remove irritating help text, when the field cannot be edited
            self.fields["email_address"].help_text = ""


class AttendanceStatusUpdateForm(forms.ModelForm):
    """
    Edits the status of an attendance.
    """

    status = forms.ChoiceField(
        choices=(
            choice for choice in Status.choices if choice[0] != Status.CREATED.value
        ),
        help_text=_(
            "Status of the attendance. If this get's updated, the attendant gets notified by mail."
        ),
    )

    class Meta:
        model = Attendance
        fields = ["status"]


class EventExportForm(forms.Form):
    grouping = forms.ChoiceField(
        choices=(
            ("accepted", _("Accepted")),
            ("waiting", _("Waitlist")),
            ("all", _("All attendances")),
        )
    )
    file_format = forms.ChoiceField(
        choices=(
            ("ods", _("Open Document Format")),
            ("xlsx", _("Excel")),
            ("csv", _("CSV")),
            ("json", _("JSON")),
        )
    )


class UserMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj: User):
        return obj.get_full_name() or obj.username


# See https://docs.djangoproject.com/en/5.0/topics/http/file-uploads/#id5
class MultipleFileInput(forms.ClearableFileInput):
    allow_multiple_selected = True
    template_name = "django/forms/widgets/multiple_file_input.html"


class MultipleFileField(forms.FileField):
    required = True

    def __init__(self, *args, **kwargs):
        kwargs.setdefault("widget", MultipleFileInput())
        super().__init__(*args, **kwargs)

    def clean(self, data, initial=None):
        single_file_clean = super().clean

        # If there are no images, and the initial text is unset and the field is required,
        # we print an error message. If the initial text is set, there are existing files,
        # so an empty input is okay.
        if self.required and len(data) == 0 and not self.widget.initial_text:
            raise ValidationError(self.error_messages["missing"], code="missing")

        if isinstance(data, (list, tuple)):
            return [single_file_clean(d, initial) for d in data]
        else:
            return [single_file_clean(data, initial)]


class EventForm(forms.ModelForm):
    organizers = UserMultipleChoiceField(
        queryset=User.objects.filter(is_active=True).order_by("first_name"),
        label=Event._meta.get_field("organizers").verbose_name,
        help_text=Event._meta.get_field("organizers").help_text,
    )
    images = MultipleFileField(
        label=_("Images"), help_text=_("Images displayed on the event.")
    )
    custom_fields_schema = forms.JSONField(
        widget=forms.HiddenInput(),
        encoder=PrettyJSONEncoder,
    )

    def clean_custom_fields_schema(self):
        schema = self.cleaned_data["custom_fields_schema"]
        if schema:
            validate_schema(schema)

        return schema

    class Meta:
        model = Event
        fields = [
            "title",
            "description",
            "start",
            "end",
            "place",
            "max_participants",
            "static_event",
            "registration_deadline",
            "price",
            "contact_email",
            "organizers",
            "waitlist_registration",
            "open_registration",
            "auto_accept_registrations",
            "custom_attendance_dates",
            "require_age",
            "draft",
            "unlisted",
            "show_attendance_count",
            "enable_markdown",
            "hide_organizer_names",
            "custom_fields_schema",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # This is arguably a bit hacky. We set the initial text of the widget
        # to a string containing the amount of images. Otherwise to an empty one.
        # This get's used to decide if the field must be filled out or not.
        # See `MultipleFileField` for more information.
        if self.instance.pk:
            self.fields["images"].widget.initial_text = _(
                "Currently {count} images uploaded."
            ).format(count=self.instance.images.count())
        else:
            self.fields["images"].widget.initial_text = ""
        # Let our image field only accept images
        self.fields["images"].widget.attrs.update({"accept": "image/*"})

        # Use datetime-local inputs for datetimes instead of text inputs
        today = (
            now()
            .replace(hour=0, minute=0, second=0, tzinfo=None)
            .isoformat(timespec="minutes")
        )

        # On empty fields, set the minimum date to today
        for field in ("start", "end", "registration_deadline"):
            configure_date_field(
                self.fields[field],
                minimum=today
                if not self.instance.pk or not getattr(self.instance, field)
                else None,
            )


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ["receive_email_notifications"]


class AttendanceFindForm(forms.Form):
    """Form for discovering all upcoming and past events given an email address."""

    email_address = forms.EmailField(
        label=_("Email Address"),
        help_text=_("Enter your email address used for event registration."),
    )
