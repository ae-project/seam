from datetime import timedelta

from django.conf import settings
from django.contrib.auth.models import User
from django.utils.text import slugify
from faker import Faker

from seam.models import Attendance, Event, Status


class BaseFactory:
    fake = Faker(settings.LANGUAGE_CODE)

    def get(self, *args, **kwargs):
        pass


class UserFactory(BaseFactory):
    def get(self) -> User:
        first_name = self.fake.first_name()
        last_name = self.fake.last_name()

        return User(
            first_name=first_name, last_name=last_name, username=self.fake.uuid4()
        )


class EventFactory(BaseFactory):
    def get(self) -> Event:
        start = self.fake.date_between(start_date="today", end_date="+1y")
        end = start + timedelta(days=self.fake.pyint(1, 10))
        events = [
            "Winterseminar",
            "Pilzseminar",
            "Kreativseminar",
            "Segelseminar",
            "Osterkongress",
            "Fahrradtour",
            "Kanutour",
            "Vorstandssitzung",
            "Vereinstreffen",
            "Arbeitswochenende",
            "Insektenseminar",
            "Exkursion ins Naturschutzgebiet",
            "Birdwatching",
            "Nachtexkursion",
            "Auslandsaustausch",
            "Konferenz der Arten",
            "Radtour",
            "Podiumsdiskussion",
        ]

        event = Event(
            title=self.fake.random_element(events),
            description=self.fake.paragraph(10) + "\n" + self.fake.paragraph(20),
            start=start,
            end=end,
            max_participants=self.fake.pyint(10, 100),
            place=self.fake.city(),
            contact_email=self.fake.ascii_email(),
            price=self.fake.random_element(
                [
                    f"{self.fake.pyint(10, 100, step=10)}€",
                    f"{self.fake.pyint(5, 15, step=1)}€/Tag",
                ]
            ),
            waitlist_registration=self.fake.boolean(chance_of_getting_true=75),
            open_registration=self.fake.boolean(chance_of_getting_true=90),
        )

        event.save()

        user_factory = UserFactory()
        for _ in range(0, self.fake.pyint(1, 2)):
            user = user_factory.get()
            user.save()

            event.organizers.add(user)

        return event


class AttendanceFactory(BaseFactory):
    def get(self, event) -> Attendance:
        first_name = self.fake.first_name()
        last_name = self.fake.last_name()

        email_verified = self.fake.boolean(chance_of_getting_true=80)

        attendance = Attendance(
            event=event,
            first_name=first_name,
            last_name=last_name,
            pronoun=self.fake.random_element(
                25 * [""]
                + ["sie/ihr", "keine Pronomen", "er/ihn", "dey/dem", "-", "egal"]
            ),
            email_address=f"{slugify(first_name)}{self.fake.random_element(('.', '-'))}{slugify(last_name)}"
            + f"@{self.fake.free_email_domain()}",
            email_verified=email_verified,
            phone_number="+49" + self.fake.msisdn()[0:10],
            age=self.fake.pyint(min_value=12, max_value=50),
            city=self.fake.city(),
            status=Status.CREATED
            if not email_verified
            else self.fake.random_element(
                2 * ["created"]
                + 10 * ["accepted"]
                + 2 * ["waiting"]
                + 1 * ["cancelled"]
            ),
        )

        attendance.save()
        Attendance.objects.filter(pk=attendance.pk).update(
            created_at=self.fake.date_time_this_month()
        )

        return attendance
