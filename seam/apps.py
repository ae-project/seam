from django.apps import AppConfig


class SeamConfig(AppConfig):
    name = "seam"

    def ready(self):
        # Connect decorated signals
        from seam import signals  # noqa
