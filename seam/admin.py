from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from django.utils.translation import gettext as _

from seam.models import Attendance, Event, Image, NegativePhrase, Profile


class EventAdmin(admin.ModelAdmin):
    list_filter = ["start", "end"]
    list_display = ["title", "start", "end", "can_register", "get_attendance_count"]
    ordering = ["end"]

    def can_register(self, obj):
        return obj.can_register()

    @admin.display(description=_("Accepted attendances"))
    def get_attendance_count(self, obj):
        return obj.get_attendance_count()

    can_register.boolean = True


class AttendanceAdmin(admin.ModelAdmin):
    list_filter = ["status", "event__title"]
    list_display = ["first_name", "last_name", "pronoun", "status", "event_title"]

    @admin.display(description=_("Event"))
    def event_title(self, obj):
        return obj.event.title


class ImageAdmin(admin.ModelAdmin):
    list_display = ["event_title", "image_file"]

    @admin.display(description=_("Event"))
    def event_title(self, obj):
        return obj.event.title


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False


class UserAdmin(BaseUserAdmin):
    inlines = [ProfileInline]


admin.site.unregister(User)
admin.site.register(User, UserAdmin)

admin.site.register(Event, EventAdmin)
admin.site.register(Attendance, AttendanceAdmin)
admin.site.register(Image, ImageAdmin)


class NegativePhraseAdmin(admin.ModelAdmin):
    list_display = ["phrase"]
    search_fields = ["phrase"]


admin.site.register(NegativePhrase, NegativePhraseAdmin)
