{% load i18n %}{% blocktrans %}Hello {{ first_name }} {{ last_name }},
in order to confirm your registration for the event "{{ title }}" click the following link:
{{ link }}
{% endblocktrans %}
