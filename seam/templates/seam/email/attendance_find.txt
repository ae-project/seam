{% load i18n %}{% blocktrans %}Hello,
you registered for the following events:
{% endblocktrans %}
{% for attendance, link in attendances %}{{ attendance.event.title }} ({{ attendance.event.start }} - {{ attendance.event.end }}): {{ link }}
{% endfor %}
