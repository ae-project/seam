from datetime import timedelta

from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.signing import Signer
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.http import Http404
from django.urls import reverse_lazy
from django.utils.functional import cached_property
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.utils.timezone import localdate, localtime, now
from django.utils.translation import gettext_lazy as _
from markdown_it import MarkdownIt

from seam.utils.markdown import MarkdownRendererText
from seam.validators import phone_number_validator


class TimestampMixin(models.Model):
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_("Created at"),
        help_text=_("Datetime of this objects creation."),
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        verbose_name=_("Updated at"),
        help_text=_("Datetime of the last update of this object."),
    )

    class Meta:
        abstract = True


class Status(models.TextChoices):
    """Attendance stati"""

    CREATED = "created", _("Created")  # Status after creation
    ACCEPTED = "accepted", _("Accepted")  # Attendant has a fixed seat at the event
    WAITING = "waiting", _("Waitlist")  # Attendant is on the waitlist
    CANCELLED = (
        "cancelled",
        _("Cancelled"),
    )  # Attendant unfortunately can't attend the event


class Event(TimestampMixin):
    title = models.CharField(
        max_length=50, verbose_name=_("Title"), help_text=_("Title of the event")
    )
    description = models.TextField(
        verbose_name=_("Description"),
        help_text=_("Description of the event."),
    )
    start = models.DateTimeField(
        verbose_name=_("Start date"),
        help_text=_(
            "Enter the start date of the event. If the time is set to 00:00, it won't be displayed."
        ),
    )
    end = models.DateTimeField(
        verbose_name=_("End date"),
        help_text=_(
            "Enter the end date of the event. If the time is set to 00:00, it won't be displayed."
        ),
    )
    place = models.CharField(
        max_length=100,
        verbose_name=_("place"),
        help_text=_("Where will the event take place?"),
    )

    max_participants = models.PositiveIntegerField(
        verbose_name=_("Maximum amount of participants."),
        help_text=_(
            "Enter the maximum amount of participants, must be greater than 1."
        ),
        validators=[MinValueValidator(1)],
        blank=True,
        null=True,
    )

    # Registration options
    static_event = models.BooleanField(
        default=False,
        verbose_name=_("Static event"),
        help_text=_(
            "Event has no registration at all. Registration is handled outside of seam."
        ),
    )

    waitlist_registration = models.BooleanField(
        default=True,
        verbose_name=_("Waitlist registration enabled"),
        help_text=_(
            "If this is checked, people can still register even if the event is full."
        ),
    )
    open_registration = models.BooleanField(
        default=False,
        verbose_name=_("Registration enabled"),
        help_text=_(
            "This enables registration for the event. "
            "If the event if full and the waitlist is disabled, the registration is still visible but disabled."
        ),
    )

    auto_accept_registrations = models.BooleanField(
        default=False,
        verbose_name=_("Auto accept registrations"),
        help_text=_(
            "Registrations are automatically accepted or set to waitlist if the event is already full. "
            "This only happens after the email address got verified."
        ),
    )

    registration_deadline = models.DateField(
        null=True,
        blank=True,
        verbose_name=_("Registration deadline"),
        help_text=_(
            "The value is displayed on the event detail page, but doesn't influence the ability to register."
        ),
    )

    price = models.CharField(
        max_length=100,
        verbose_name=_("Price"),
        help_text=_("Enter the estimated costs of the event. E.g. 5€/day or 50€"),
    )

    organizers = models.ManyToManyField(
        User,
        verbose_name=_("Organizers"),
        help_text=_(
            "List of all users, being able to access and edit the event. Shown on the event's detail page."
        ),
    )

    contact_email = models.EmailField(
        verbose_name=_("Contact email"),
        blank=True,
        help_text=_("Contact email listed on the detail page of the event."),
    )

    # Display options
    draft = models.BooleanField(
        default=False,
        verbose_name=_("Draft"),
        help_text=_(
            "If the event is marked as draft it's only visible to administrators and the events organizers."
        ),
    )

    unlisted = models.BooleanField(
        default=False,
        verbose_name=_("Unlisted"),
        help_text=_(
            "Hide event from the list view. Note that the event can still be accessed by the direct url."
        ),
    )

    show_attendance_count = models.BooleanField(
        default=False,
        verbose_name=_("Show Attendance count"),
        help_text=_(
            "Displays the amount of accepted attendances on the events detail page."
        ),
    )

    enable_markdown = models.BooleanField(
        default=False,
        verbose_name=_("Enable Markdown"),
        help_text=_("Renders the event description with Markdown (GFM)."),
    )

    hide_organizer_names = models.BooleanField(
        default=False,
        verbose_name=_("Hide organizer names"),
        help_text=_(
            "If enabled the names of the organizers are not shown on the detail page. "
            "If unchecked the first name of the organizers will be visible."
        ),
    )

    # Attendance options
    custom_attendance_dates = models.BooleanField(
        default=False,
        verbose_name=_("Custom attendance arrival/depature datetimes"),
        help_text=_(
            "Enables attendants to to specify an arrival date and departure date on registration."
        ),
    )

    require_age = models.BooleanField(
        default=False,
        verbose_name=_("Require age"),
        help_text=_("Specifies if attendants must supply an age during registration."),
    )

    custom_fields_schema = models.JSONField(
        blank=True,
        null=True,
        verbose_name=_("Custom Fields schema"),
        help_text=_("Add your custom fields here."),
    )

    class Meta:
        verbose_name = _("Event")
        verbose_name_plural = _("Events")

    def __str__(self):
        return f"{self.title} - ({self.start} - {self.end})"

    def get_absolute_url(self):
        return reverse_lazy("event_detail", kwargs={"pk": self.pk})

    def clean(self):
        if not self.max_participants and not self.static_event:
            raise ValidationError(
                {
                    "max_participants": _(
                        "Max participants must be set if the event is not static."
                    )
                },
                code="required",
            )

    def can_access_admin(self, user: User | None) -> bool:
        """Returns true if the given user is allowed access to the event."""

        return bool(
            user
            and user.is_authenticated
            and (user.is_superuser or self.organizers.filter(pk=user.pk).exists())
        )

    def can_register(self) -> bool:
        """Returns registration status."""

        # Registrations must be open
        # and there must be a waitlist or enough spaces available

        # We count created attendances towards our max participants
        return (
            not self.draft
            and not self.static_event
            and self.open_registration
            and not self.has_ended()
            and (
                self.waitlist_registration
                or self.attendances.filter(
                    status__in=(Status.ACCEPTED, Status.CREATED), email_verified=True
                ).count()
                < self.max_participants
            )
        )

    def waitlist_warning(self) -> bool:
        """Returns true if the next attendance is likely getting on the waitlist."""
        return (
            self.attendances.filter(
                status__in=(Status.ACCEPTED, Status.CREATED), email_verified=True
            ).count()
            > self.max_participants
        )

    def has_ended(self) -> bool:
        """Returns true if the event is in the past."""
        return self.end < now()

    def get_attendance_count(self):
        """Returns the amount of accepted attendances."""
        return self.attendances.filter(status=Status.ACCEPTED).count()

    def text_description(self):
        """Returns the textual representation of the description. Markdown symbols are stripped."""
        if not self.enable_markdown:
            return self.description
        else:
            parser = MarkdownIt(renderer_cls=MarkdownRendererText)
            return parser.render(self.description)

    def get_custom_field_names(self) -> dict[str, str]:
        """Returns a mapping between field names and their respective labels."""
        return (
            {key: value["label"] for key, value in self.custom_fields_schema.items()}
            if self.custom_fields_schema
            else {}
        )

    @cached_property
    def days(self):
        """We can't use a set here, they need to be ordered."""
        start = localdate(self.start)
        end = localdate(self.end)
        return [start + timedelta(days=day) for day in range((end - start).days + 1)]

    @cached_property
    def attendances_per_day(self):
        """Returns the attendance count per day."""
        result = []
        attendances = self.attendances.filter(status=Status.ACCEPTED)

        for day in self.days:
            result.append(
                sum((1 for attendance in attendances if day in attendance.days))
            )
        return result

    @cached_property
    def attendance_days(self):
        """Returns the cumulative attendance days.

        Note: arrival/depature dates are counted as one day each.
        """
        if not self.custom_attendance_dates:
            return (
                len(self.days) * self.attendances.filter(status=Status.ACCEPTED).count()
            )
        return sum(
            len(attendance.days)
            for attendance in self.attendances.filter(status=Status.ACCEPTED)
        )

    @cached_property
    def whole_day_event(self) -> bool:
        """True if the event starts and ends with a full day."""

        def is_date(value) -> bool:
            localized = localtime(value)
            return (
                localized.hour == 0 and localized.minute == 0 and localized.second == 0
            )

        return is_date(self.start) and is_date(self.end)


class Attendance(TimestampMixin):
    event = models.ForeignKey(
        Event, on_delete=models.CASCADE, related_name="attendances"
    )

    first_name = models.CharField(
        max_length=50,
        verbose_name=_("First name"),
        help_text=_("Enter your first name here."),
    )
    last_name = models.CharField(
        max_length=50,
        verbose_name=_("Last name"),
        help_text=_("Enter your last name here."),
    )
    pronoun = models.CharField(
        max_length=50,
        verbose_name=_("Pronoun"),
        help_text=_(
            "Which pronoun would you like to be addressed with? E.g. They/Them, She/Her, He/Him"
        ),
        blank=True,
    )

    age = models.PositiveIntegerField(
        validators=[MaxValueValidator(100)],
        verbose_name=_("Age"),
        help_text=_("How old are you?"),
        blank=True,
        null=True,
    )
    email_address = models.EmailField(
        verbose_name=_("Email Address"), help_text=_("Enter your email address.")
    )
    city = models.CharField(
        blank=True,
        max_length=50,
        verbose_name=_("City"),
        help_text=_("Which city do you live in?"),
    )

    phone_number = models.CharField(
        validators=[phone_number_validator],
        verbose_name=_("Phone number"),
        blank=True,
        help_text=_("Enter your phone number in the format +1274921332947."),
    )

    food_intolerances = models.TextField(
        verbose_name=_("Food intolerances"),
        blank=True,
        help_text=_(
            "Do you have any food intolerances, like lactose intolerance or allergies?"
        ),
    )
    notes = models.TextField(
        verbose_name=_("Additional information"),
        blank=True,
        help_text=_("Enter additional information for the organizers."),
    )

    status = models.CharField(
        max_length=10, choices=Status, blank=False, verbose_name=_("Status")
    )
    email_verified = models.BooleanField(
        default=False, verbose_name=_("Email verified")
    )
    share_contact_information = models.BooleanField(
        default=False,
        verbose_name=_("Share contact information"),
        help_text=_(
            "If this is enabled, your name, pronoun, place, arrival, "
            " depature dates and email address will be visible to other "
            "attendees for carpooling."
        ),
    )

    start = models.DateTimeField(
        verbose_name=_("Arrival date"),
        help_text=_(
            "Enter the date and time of your arrival at the event. "
            "If the time is set to 00:00, we assume you arrive at some point that day."
        ),
        blank=True,
        null=True,
    )
    end = models.DateTimeField(
        verbose_name=_("Depature date"),
        help_text=_(
            "Enter the date and time you leave the event. "
            "If the time is set to 00:00, we assume you depart at some point that day."
        ),
        blank=True,
        null=True,
    )

    custom_fields_data = models.JSONField(blank=True, null=True)

    def full_name(self) -> str:
        """Returns the full name including pronouns of the attendant."""
        name = f"{self.first_name} {self.last_name}"
        return name + f" ({self.pronoun})" if self.pronoun else name

    def generate_access_id(self) -> str:
        """Generates a link used to edit and verify an event attendance."""
        signer = Signer()
        return urlsafe_base64_encode(signer.sign(str(self.pk)).encode())

    @staticmethod
    def get_from_access_id(payload: str) -> "Attendance":
        """Returns the underlying attendance for a given accces id.

        Raises a 404 if the `payload` is malformed or the attendance doesn't exist.
        """
        signer = Signer()

        try:
            return Attendance.objects.get(
                pk=signer.unsign(urlsafe_base64_decode(payload).decode())
            )
        except (Attendance.DoesNotExist, UnicodeDecodeError, ValueError):
            raise Http404

    def get_absolute_url(self):
        return reverse_lazy(
            "attendance_detail", kwargs={"attendance": self.generate_access_id()}
        )

    @cached_property
    def days(self):
        """Returns a set of days."""
        start = localdate(self.start or self.event.start)
        end = localdate(self.end or self.event.end)
        return set(start + timedelta(days=day) for day in range((end - start).days + 1))

    def get_custom_fields(self):
        """Returns a k, v mapping of the custom fields data based on the event schema."""
        return (
            {
                field_name: self.custom_fields_data.get(field_name, None)
                if self.custom_fields_data
                else None
                for field_name, field_schema in self.event.custom_fields_schema.items()
            }
            if self.event.custom_fields_schema
            else {}
        )

    def get_custom_fields_human_readable(self):
        """Returns custom fields data based on the current schema on the event."""
        return (
            (self.event.custom_fields_schema[field_name]["label"], field_value)
            for field_name, field_value in self.get_custom_fields().items()
        )

    class Meta:
        verbose_name = _("Attendance")
        verbose_name_plural = _("Attendances")

    def __str__(self):
        return _("Registration of {user} to {title}").format(
            title=self.event.title, user=self.full_name()
        )


def image_upload_to(instance, filename):
    return f"unprocessed/{ filename }"


class Image(models.Model):
    image_file = models.ImageField(
        verbose_name=_("Image file"),
        help_text=_("Images that are displayed on the event."),
        upload_to=image_upload_to,
    )

    event = models.ForeignKey(
        Event,
        on_delete=models.CASCADE,
        related_name="images",
        verbose_name=_("Images"),
        help_text=_("Images for an event."),
    )

    status = models.CharField(
        max_length=20,
        choices=(
            ("unprocessed", _("Unprocessed")),
            ("raw", _("Raw")),
            ("processed", _("Processed")),
        ),
        blank=False,
        default="unprocessed",
        verbose_name=_("Image processing status"),
    )

    @cached_property
    def processed(self):
        return self.status == "processed"

    def preview_url(self) -> str:
        """Returns the path to the image file that get's used as the preview image."""
        return (
            settings.IMAGE_PREVIEW_URL.format(pk=self.pk)
            if self.processed
            else self.image_file.url
        )

    class Meta:
        verbose_name = _("Image")
        verbose_name_plural = _("Images")

    def __str__(self):
        return _("Image of {title}").format(title=self.event.title)


class NegativePhrase(models.Model):
    phrase = models.CharField(
        max_length=50,
        verbose_name=_("Phrase"),
        help_text=_("A phrase that indicates 'no' (e.g., 'Nein', 'keine', 'Nö')"),
        unique=True,
    )

    class Meta:
        verbose_name = _("Negative phrase")
        verbose_name_plural = _("Negative phrases")

    def __str__(self):
        return self.phrase


class Profile(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )

    receive_email_notifications = models.BooleanField(
        default=True,
        verbose_name=_("Receive email notifications"),
        help_text=_(
            "If enabled you'll receive emails for new attendances to your events."
        ),
    )

    def __str__(self):
        return _("Profile of {username}").format(username=self.user.username)
