# Generated by Django 5.0.1 on 2024-02-01 22:46

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("seam", "0021_rename_readonly_event_event_static_event"),
    ]

    operations = [
        migrations.AlterField(
            model_name="event",
            name="static_event",
            field=models.BooleanField(
                default=False,
                help_text="Event has no registration at all. Registration is handled outside of seam.",
                verbose_name="Static event",
            ),
        ),
    ]
