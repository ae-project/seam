# Generated by Django 5.0.1 on 2024-02-01 22:34

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("seam", "0020_remove_event_image"),
    ]

    operations = [
        migrations.RenameField(
            model_name="event",
            old_name="readonly_event",
            new_name="static_event",
        ),
    ]
