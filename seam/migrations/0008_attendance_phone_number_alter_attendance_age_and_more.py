# Generated by Django 5.0.1 on 2024-01-19 10:09

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("seam", "0007_alter_event_organizers"),
    ]

    operations = [
        migrations.AddField(
            model_name="attendance",
            name="phone_number",
            field=models.CharField(
                blank=True,
                help_text="Enter your phone number in the format +1274921332947.",
                validators=[
                    django.core.validators.RegexValidator(
                        message="Phone number must be entered in the format +12345869. Up to 15 digits allowed.",
                        regex="^\\+?1?\\d{9,15}$",
                    )
                ],
                verbose_name="Phone number",
            ),
        ),
        migrations.AlterField(
            model_name="attendance",
            name="age",
            field=models.PositiveIntegerField(
                help_text="How old are you?",
                validators=[django.core.validators.MaxValueValidator(100)],
                verbose_name="Age",
            ),
        ),
        migrations.AlterField(
            model_name="attendance",
            name="city",
            field=models.CharField(
                help_text="Which city do you live in?",
                max_length=50,
                verbose_name="City",
            ),
        ),
        migrations.AlterField(
            model_name="attendance",
            name="email_address",
            field=models.EmailField(
                help_text="Enter your email address.",
                max_length=254,
                verbose_name="Age",
            ),
        ),
        migrations.AlterField(
            model_name="attendance",
            name="first_name",
            field=models.CharField(
                help_text="Enter your first name here.",
                max_length=50,
                verbose_name="First name",
            ),
        ),
        migrations.AlterField(
            model_name="attendance",
            name="last_name",
            field=models.CharField(
                help_text="Enter your last name here.",
                max_length=50,
                verbose_name="Last name",
            ),
        ),
        migrations.AlterField(
            model_name="attendance",
            name="pronoun",
            field=models.CharField(
                help_text="Which pronoun would you like to be addressed with? E.g. They/Them, She/Her, He/Him",
                max_length=50,
                verbose_name="Pronoun",
            ),
        ),
    ]
