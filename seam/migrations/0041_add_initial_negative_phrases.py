from django.db import migrations


def add_initial_phrases(apps, schema_editor):
    NegativePhrase = apps.get_model("seam", "NegativePhrase")

    phrases = [
        "nein",
        "keine",
        "nö",
        "no",
        "/",
        "-",
        "nicht",
        "kein",
        "nichts",
    ]

    for phrase in phrases:
        NegativePhrase.objects.create(phrase=phrase)


def remove_phrases(apps, schema_editor):
    NegativePhrase = apps.get_model("seam", "NegativePhrase")
    NegativePhrase.objects.all().delete()


class Migration(migrations.Migration):
    dependencies = [
        ("seam", "0040_add_negative_phrases"),
    ]

    operations = [
        migrations.RunPython(add_initial_phrases, remove_phrases),
    ]
