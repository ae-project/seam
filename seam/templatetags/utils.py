from datetime import datetime

from django import template
from django.utils.safestring import mark_safe
from django.utils.timezone import localtime
from django.utils.translation import gettext as _

register = template.Library()


@register.filter(name="datetify")
def datetify(value: datetime):
    """Returns a date instead of a datetime if the time is at 00:00:00"""
    # Fail gracefully on missing inputs
    if not value:
        return ""

    value = localtime(value)
    return (
        value.date()  # value is already localized
        if value.hour == 0 and value.minute == 0 and value.second == 0
        else value
    )


@register.filter(name="checked")
def checked(value: bool) -> str:
    """Returns checkmark or x for a bool."""
    result = (
        '<i class="bi bi-check" aria-label="' + _("Yes") + '"></i>'
        if value
        else '<i class="bi bi-x" aria-label="' + _("No") + '"></i>'
    )
    return mark_safe(result)


@register.filter(name="split")
def split(value: str, key: str) -> list[str]:
    """Splits a string on a given to and returns a list of strings."""
    return value.split(key)


@register.filter(name="get")
def get(data: dict, key):
    """Return a dict value based on its key."""
    return data[key]
