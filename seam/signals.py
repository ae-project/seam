from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from seam.models import Profile


@receiver(post_save, sender=User)
def add_profile_to_user(sender, **kwargs):
    """Adds a profile to a newly created user."""
    if kwargs["created"]:
        Profile.objects.create(user=kwargs["instance"])
